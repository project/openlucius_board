<?php
/**
 * @file
 * This file contains the functions which are required for rendering the pages.
 */

/**
 * Function for returning the page title.
 *
 * @param \stdClass $node
 *   The node object to be used for creating the title.
 *
 * @return string
 *   Returns the page title for this board.
 */
function openlucius_board_title(\stdClass $node) {
  return t('Board for @title', array('@title' => $node->title));
}

/**
 * Function for returning the group board.
 *
 * @param \stdClass|NULL $object
 *   The node object to be used for creating the board.
 *
 * @return string
 *   Returns the rendered board.
 */
function openlucius_board_page($object = NULL) {
  global $user;

  // Check if this is a group board.
  if (!empty($object->nid)) {
    $board = new OpenluciusBoard($object->nid, 'node');
  }
  // Check if this is a user board.
  elseif (!empty($object->uid)) {

    // Fetch associated users to verify that you may see this users board.
    $associated = openlucius_core_fetch_associated_users(TRUE);

    // Check if this user is either viewing is own profile,
    // is associated with the user he / she is trying to view or is admin.
    if ($user->uid == $object->uid || in_array($object->uid, $associated) || $user->uid == 1) {
      $board = new OpenluciusBoard($object->uid, 'user');
    }
  }
  // Check if this method was called without parameters.
  elseif ($object == NULL) {
    $board = new OpenluciusBoard($user->uid, 'user');
  }

  // Check if we have a board.
  if (!empty($board)) {
    return $board->render();
  }
  else {
    drupal_access_denied();
    drupal_exit();
  }
}

/**
 * Function for returning the modal comment form and stream.
 *
 * @param bool $js
 *   Whether js should be used or not.
 * @param \stdClass $node
 *   The node object to be used for building the thread and comment form.
 * @param string $view_type
 *   The type of modal to be rendered, either comment or node.
 *
 * @return string
 *   Returns the rendered board.
 */
function openlucius_board_modal_comment_form($js = FALSE, \stdClass $node, $view_type = 'comment-view') {
  ctools_include('modal');
  ctools_include('ajax');
  ctools_include('comment.pages', 'comment', '');

  // Build the form id.
  $form_id = 'comment_node_' . $node->type . '_form';

  if (!$js) {
    drupal_goto('node/' . $node->nid);
  }

  // Check what type of modal we need.
  $is_comment_view = $view_type == 'comment-view';
  $comment = (object) array('nid' => $node->nid);

  // Set the modal values.
  $modal_values = array(
    'nid' => $node->nid,
    'title' => $node->title,
  );

  // Allow other modules to alter the title of the modal.
  drupal_alter('openlucius_board_board_modal_comment_form_title', $modal_values);

  $form_state = array(
    'title' => $modal_values['title'],
    'ajax' => TRUE,
    'comment' => $comment,
    'openlucius_board_comment' => TRUE,
  );

  // Set the build info.
  $form_state['build_info']['args'] = isset($form_state['build_info']['args']) ? $form_state['build_info']['args'] : array($comment);
  form_load_include($form_state, 'inc', 'comment', 'comment.pages');

  $commands = ctools_modal_form_wrapper($form_id, $form_state);

  // Attach behaviours to newly added content.
  $commands[] = array(
    'command' => 'attachBehaviours',
  );

  // Add no scroll class to html.
  $commands[] = ajax_command_invoke('html', 'addClass', array(
    'openlucius-core-no-scroll',
  ));

  // Trigger the individuals method in the theme behaviours.
  $commands[] = array(
    'command' => 'showOtherIndividuals',
    'data'    => TRUE,
  );

  // This will allow us to scroll to the todo form.
  if ($is_comment_view) {
    $commands[] = array(
      'command' => 'commentScrollToForm',
      'data'    => TRUE,
    );
  }

  // Check if the form was submitted.
  if (!empty($form_state['executed'])) {

    // Clear any messages.
    if (!empty($_SESSION['messages'])) {
      unset($_SESSION['messages']);
    }

    $commands = array();

    ctools_add_js('ajax-responder');

    $commands[] = ctools_modal_command_dismiss();

    // Remove no scroll class from html.
    $commands[] = ajax_command_invoke('html', 'removeClass', array(
      'openlucius-core-no-scroll',
    ));

    // Fetch new node.
    $node = node_load($node->nid);

    // This will allow us to reload the todo after an edit.
    $commands[] = array(
      'command' => 'reloadTodoInline',
      'data'    => openlucius_board_extract_values($node),
    );
  }

  print ajax_render($commands);
  drupal_exit();
}

/**
 * Callback form for the modal node edit.
 *
 * @param bool $js
 *   Whether js is to be used.
 * @param \stdClass $node
 *   The current node to be used for the form.
 *
 * @return array|mixed
 *   Returns the rendered form.
 */
function openlucius_board_modal_form($js = FALSE, \stdClass $node) {
  ctools_include('node.pages', 'node', '');
  ctools_include('modal');
  ctools_include('ajax');

  // Build node form name.
  $node_form = $node->type . '_node_form';

  // Check if js is being used.
  if (!$js) {
    drupal_goto('node/' . $node->nid . '/edit');
  }

  // Fetch node type name.
  $type_name = node_type_get_name($node);

  $form_state = array(
    'title' => t('<em>Edit @type</em>', array('@type' => $type_name)),
    'ajax'  => TRUE,
    'node'  => $node,
    'only_lists' => TRUE,
    'openlucius_board_modal' => TRUE,
  );

  $form_state['build_info']['args'] = isset($form_state['build_info']['args']) ? $form_state['build_info']['args'] : array($node);
  form_load_include($form_state, 'inc', 'node', 'node.pages');

  $commands = ctools_modal_form_wrapper($node->type . '_node_form', $form_state);

  // Add no scroll class to html.
  $commands[] = ajax_command_invoke('html', 'addClass', array(
    'openlucius-core-no-scroll',
  ));

  // Trigger the individuals method in the theme behaviours.
  $commands[] = array(
    'command' => 'showOtherIndividuals',
    'data'    => TRUE,
  );

  // Check if the form was submitted.
  if (!empty($form_state['executed'])) {

    // Clear any messages.
    if (!empty($_SESSION['messages'])) {
      unset($_SESSION['messages']);
    }

    // Overwrite commands.
    $commands = array();

    // Add ajax responder for executing commands.
    ctools_add_js('ajax-responder');

    // Add command for closing the modal.
    $commands[] = ctools_modal_command_dismiss();

    // Fetch new node.
    $node = node_load($node->nid);

    // This will allow us to reload the todo after an edit.
    $commands[] = array(
      'command' => 'reloadTodoInline',
      'data'    => openlucius_board_extract_values($node),
    );

    // Remove no scroll class from html.
    $commands[] = ajax_command_invoke('html', 'removeClass', array(
      'openlucius-core-no-scroll',
    ));
  }

  print ajax_render($commands);
  drupal_exit();
}

/**
 * Callback form for the modal node edit.
 *
 * @param bool $js
 *   Whether js is to be used.
 * @param \stdClass $node
 *   The group node for which a node has to be added.
 * @param int $label
 *   The todo status this todo should have.
 * @param string $type
 *   The type of node to be added.
 *
 * @return array|mixed
 *   Returns the rendered form.
 */
function openlucius_board_modal_node_add_todo_form($js = FALSE, \stdClass $node, $label, $type) {
  global $user, $language;

  ctools_include('node.pages', 'node', '');
  ctools_include('modal');
  ctools_include('ajax');

  // Build node form name.
  $node_form = $type . '_node_form';

  // Build simple empty object.
  $empty_node = new stdClass();

  // Add the type and language.
  $empty_node->type = $type;
  $empty_node->language = $language->language;

  // Prepare the node object for node form.
  node_object_prepare($empty_node);

  // Set the group.
  $empty_node->field_shared_group_reference[LANGUAGE_NONE][0]['nid'] = $node->nid;

  // Clients may not set the label.
  if (!openlucius_core_user_is_client()) {

    // Set the status.
    $empty_node->field_todo_label[LANGUAGE_NONE][0]['tid'] = $label;
  }

  // Add the required fields.
  $empty_node->uid = $user->uid;
  $empty_node->name = isset($user->name) ? $user->name : '';

  //field_todo_user_reference
  module_load_include('inc', 'openlucius_core', 'includes/openlucius_core_move_node');
  $lists = openlucius_core_load_todo_list($node->nid);
  $empty_node->field_todo_list_reference[LANGUAGE_NONE][0]['nid'] = array(key($lists));

  // Check if js is being used.
  if (!$js) {
    drupal_goto('node/add/ol-todo/' . $node->nid);
  }

  $form_state = array(
    'title' => t('Add Task'),
    'ajax'  => TRUE,
    'node'  => $empty_node,
    'name' => (isset($user->name) ? $user->name : ''),
    'only_lists' => TRUE,
    'openlucius_board_modal' => TRUE,
  );

  $form_state['build_info']['args'] = isset($form_state['build_info']['args']) ? $form_state['build_info']['args'] : array($empty_node);
  form_load_include($form_state, 'inc', 'node', 'node.pages');

  $commands = ctools_modal_form_wrapper($node_form, $form_state);

  // Add CSS class to Modal-Frame.
  $commands[] = ajax_command_invoke('#modalContent', 'addClass', array(
    'openlucius-task-modal',
  ));

  // Add no scroll class to html.
  $commands[] = ajax_command_invoke('html', 'addClass', array(
    'openlucius-core-no-scroll',
  ));

  // Trigger the individuals method in the theme behaviours.
  $commands[] = array(
    'command' => 'showOtherIndividuals',
    'data'    => TRUE,
  );

  // Check if the form was submitted.
  if (!empty($form_state['executed'])) {

    // Clear any messages.
    if (!empty($_SESSION['messages'])) {
      unset($_SESSION['messages']);
    }

    // Load the node from the form_state.
    $node = node_load($form_state['node']->nid);

    // Overwrite commands.
    $commands = array();

    // Add ajax responder for executing commands.
    ctools_add_js('ajax-responder');

    // Add command for closing the modal.
    $commands[] = ctools_modal_command_dismiss();

    // Extract node values.
    $node_values = openlucius_board_extract_values($node);

    // Theme item for board.
    $item = theme('openlucius_board_item', $node_values);

    // Return with command.
    $commands[] = array(
      'command' => 'addNewTodoInline',
      'data'    => array(
        'html' => $item,
        'tid' => $label,
      ),
    );

    // Remove no scroll class from html.
    $commands[] = ajax_command_invoke('html', 'removeClass', array(
      'openlucius-core-no-scroll',
    ));
  }

  print ajax_render($commands);
  drupal_exit();
}

/**
 * Function for updating the todo.
 *
 * @param \stdClass $node
 *   The todo which has to be updated.
 */
function openlucius_board_update_todo(\stdClass $node) {

  // Check if we have a valid token and whether this user may edit the node.
  if (drupal_valid_token($_POST['token']) && node_access('update', $node)) {

    // Clear node caches.
    cache_clear_all("field:node:$node->nid", 'cache_field');

    // Use the entityMetadataWrapper for easy access.
    $wrapper = entity_metadata_wrapper('node', $node);

    // Check if the term id is set.
    if (!empty($_POST['tid'])) {
      $wrapper->field_todo_label->set((int) check_plain($_POST['tid']));
    }

    // Check if uid is set.
    if (!empty($_POST['uid'])) {
      $wrapper->field_todo_user_reference->set((int) check_plain($_POST['uid']));
    }

    // Check if the due date is set.
    if (!empty($_POST['date'])) {

      // Fetch clean and create timestamp from sent date.
      $date = strtotime(check_plain($_POST['date']));

      // Set the due date.
      $wrapper->field_todo_due_date_singledate->set((int) $date);
    }

    // This is not node specific but group specific.
    if (!empty($_POST['order'])) {
      $order = drupal_json_decode($_POST['order']);

      // Remove any weights for the existing nodes.
      db_delete('openlucius_board_node_weights')->condition('nid', $order, 'IN')->execute();

      // Add the weights.
      foreach ($order as $weight => $nid) {
        db_insert('openlucius_board_node_weights')->fields(array('nid', 'weight'), array($nid, $weight))->execute();
      }
    }

    // Save the node.
    $wrapper->save();
  }

  // Load ctools requirements.
  ctools_include('modal');
  ctools_include('ajax');

  // Reload node object.
  $node = node_load($node->nid);
  $meta = openlucius_board_extract_values($node);

  // Output the object for replacement.
  drupal_json_output(array('data' => $meta));
  drupal_exit();
}

/**
 * Function to fetch user select via ajax.
 *
 * @param \stdClass $node
 *   The group node for which the ajax selector is requested.
 */
function openlucius_board_group_user_select(\stdClass $node) {

  // Fetch the available group users.
  $form = array(
    'select' => array(
      '#type'    => 'select',
      '#options' => array('_none' => t('- None -')) + openlucius_core_fetch_group_users($node->nid),
    ),
  );

  // Render the html.
  $html = drupal_render($form);

  // Output the object for replacement.
  drupal_json_output($html);
  drupal_exit();
}
