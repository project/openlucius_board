<?php
/**
 * @file
 * openlucius_board.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function openlucius_board_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
