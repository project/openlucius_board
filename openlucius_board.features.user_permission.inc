<?php
/**
 * @file
 * openlucius_board.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function openlucius_board_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'drag and drop board items'.
  $permissions['drag and drop board items'] = array(
    'name' => 'drag and drop board items',
    'roles' => array(
      'admin' => 'admin',
      'can create groups' => 'can create groups',
      'openlucius authenticated user' => 'openlucius authenticated user',
    ),
    'module' => 'openlucius_board',
  );

  return $permissions;
}
