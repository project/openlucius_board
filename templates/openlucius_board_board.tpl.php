<?php
/**
 * @file
 * This file contains the template for the board page.
 */
?>
<?php if (!empty($users)): ?>
  <div id="user-selection" class="hide">
    <?php print $users; ?>
  </div>
<?php endif; ?>

<?php if (!empty($filter)): ?>
  <?php print $filter; ?>
<?php endif; ?>

<div id="openlucius-board" data-token="<?php print $token; ?>" class="<?php if (isset($user_is_client) && $user_is_client): ?>user-is-client<?php endif; ?>">
  <?php foreach ($statuses as $tid => $status): ?>
    <div class="openlucius-board-column <?php if (empty($lists[$tid])): ?>list-is-empty<?php endif; ?>" data-tid="<?php print $tid; ?>">
      <h2><?php print $status; ?> <span class="badge board-column-counter"></span>
        <i class="fa fa-compress"></i>

        <?php if (isset($user_is_client) && !$user_is_client): ?>
          <?php if (!empty($modal_add_top[$tid])): ?>
            <?php print $modal_add_top[$tid]; ?>
          <?php endif; ?>
        <?php endif; ?>
      </h2>

      <?php if (isset($lists[$tid])): ?>
        <?php foreach ($lists[$tid] as $items): ?>
          <?php foreach ($items as $item): ?>
            <?php if(!is_array($item)): ?>
              <?php print $item; ?>
            <?php else: ?>
              <?php foreach ($item as $sub_item): ?>
                <?php print $sub_item; ?>
              <?php endforeach; ?>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
      <?php endif; ?>
    </div>
  <?php endforeach; ?>
</div>
