<?php
/**
 * @file
 * This file contains the openlucius Kanban item template.
 */
?>
<div class="openlucius-board-item well <?php if (!empty($due_date['class'])): print $due_date['class']; endif; ?>" data-group-nid="<?php print $group_id; ?>" data-nid="<?php print $nid; ?>" data-weight="<?php print $weight; ?>">
  <?php if (!empty($before)): ?>
    <?php print $before; ?>
  <?php endif; ?>
  <?php if (!empty($group)): ?>
    <span class="label">
      <?php print $group; ?>
      <?php if (!$hide_label): ?>> <?php print $list; ?> <?php endif; ?>
    </span>
  <?php elseif (!empty($list) && !$hide_label): ?>
    <span class="label"><?php print $list; ?></span>
  <?php endif; ?>
  <?php print $edit_link; ?>
  <div class="board-header">
    <p data-attr="title"><?php print $title; ?></p>
  </div>
  <div class="board-meta">
    <?php print $meta; ?>
  </div>
  <?php if (!empty($after)): ?>
    <?php print $after; ?>
  <?php endif; ?>
</div>
