<?php
/**
 * @file
 * This file contains the metadata information for the board items.
 */
?>
<?php if (!empty($comments)): ?>
  <?php print $comments['link']; ?>
<?php endif; ?>

<?php if (!empty($attachments)): ?>
  <div class="badge" title="<?php print $attachments['title']; ?>">
    <span class="glyphicon glyphicon-paperclip"></span>
    <span class="badge-text"><?php print $attachments['count']; ?></span>
  </div>
<?php endif; ?>

<?php if (!empty($due_date)): ?>
  <a href="#" data-toggle="popover" class="badge-text board-date-picker badge" data-year="<?php print $due_date['year']; ?>" data-month="<?php print $due_date['month']; ?>" data-day="<?php print $due_date['day']; ?>">
    <span class="fa fa-calendar"></span>
    <?php print $due_date['date']; ?>
  </a>
<?php endif; ?>

<?php if (!empty($show_clients)): ?>
  <div class="badge" title="<?php print $show_clients['title']; ?>">
    <span class="<?php print $show_clients['class']; ?>"></span>
    <?php print $show_clients['label']; ?>
  </div>
<?php endif; ?>

<?php if (!empty($account)): ?>
  <button tabindex="0" role="button" data-toggle="popover" class="badge assigned-to <?php if (!empty($account['unassigned'])): ?>unassigned<?php endif; ?>" data-uid="<?php print $account['uid']; ?>">
    <?php print $account['picture']; ?>
  </button>
<?php endif; ?>
